from pyspark import SparkConf, SparkContext

number_cores = 2
memory_gb = 4
# Create a configuration object and
# set the name of the application
conf = (
    SparkConf()
    .setAppName("SparkExample")
    .setMaster(f"local[{number_cores}]")
    .set("spark.driver.memory", f"{memory_gb}g")
)
# Create a Spark Context object
sc = SparkContext(conf=conf)

text = sc.textFile("../input/book.txt")

word_count = text.flatMap(lambda line: line.split(" ")) \
    .map(lambda word: (word.replace(",", "").replace(".", "").replace(":", "").replace("?", ""), 1)) \
    .reduceByKey(lambda x, y: x + y) \
    .filter(lambda k: k[0].startswith('co')) \
    .sortBy(lambda v: v[1], ascending=False)

print(f"Amount of filtered records: {word_count.count()}")
print(f"Top quantity: {word_count.take(1)}")

# [0] - first element in the list, [1] - it's value
top_quantity = word_count.take(1)[0][1]

word_count = word_count.filter(lambda v: v[1] > top_quantity * 0.8)
print(f"Amount of filtered second time records: {word_count.count()}")

# taking the words to write to the file
word_count.map(lambda x: x[0]).saveAsTextFile("../output/result.txt")

sc.stop()
