from player import Player


class PlayerBuilder:

    def __init__(self):
        self.amount_of_prizes = 0
        self.win_rate = 0
        self.team_name = None
        self.nickname = None
        self.accuracy = 0

    def with_accuracy(self, accuracy):
        self.accuracy = accuracy
        return self

    def with_nickname(self, nickname):
        self.nickname = nickname
        return self

    def with_team_name(self, team_name):
        self.team_name = team_name
        return self

    def with_win_rate(self, win_rate):
        self.win_rate = win_rate
        return self

    def with_amount_of_prizes(self, amount_of_prizes):
        self.amount_of_prizes = amount_of_prizes
        return self

    def build(self):
        return Player(self.nickname, self.team_name, self.accuracy, self.win_rate, self.amount_of_prizes)


player = PlayerBuilder\
    .with_accuracy(12)\
    .with_nickname('john')\
    .with_team_name("team #1")\
    .with_amount_of_prizes(1)\
    .with_win_rate(50)\
    .build()

print(vars(player))
print(player.nickname)
print(player.nickname)

