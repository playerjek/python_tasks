class Player:

    def __init__(self, accuracy=0, win_rate=0, amount_of_prizes=0):
        self.accuracy = accuracy
        # self.nickname = nickname
        # self.team_name = team_name
        self.win_rate = win_rate
        self.amount_of_prizes = amount_of_prizes

    def __set_accuracy(self, accuracy):
        self._accuracy = accuracy

    def __set__nickname(self, nickname):
        self._nickname = nickname

    def __set__team_name(self, team_name):
        self._team_name = team_name

    def __set__win_rate(self, win_rate):
        self._win_rate = win_rate

    def __set__amount_of_prizes(self, amount_of_prizes):
        self._amount_of_prizes = amount_of_prizes

    def __get__nickname(self):
        return self._nickname

    accuracy = property(None, __set_accuracy, None, "This is property documentation")
    nickname = property(__get__nickname, __set__nickname, None, "This is property documentation")
    team_name = property(None, __set__team_name, None, "This is property documentation")
    win_rate = property(None, __set__win_rate, None, "This is property documentation")
    amount_of_prizes = property(None, __set__amount_of_prizes, None, "This is property documentation")
