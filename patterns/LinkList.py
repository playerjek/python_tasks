class LinkList:

    first_element = None
    last_element = None

    def __init__(self, *args):
        previous_item = None

        for i, item in enumerate(args):
            if previous_item is not None:
                ref = Ref(item, left=self.last_element)
                self.last_element.right = ref
            else:
                ref = self.first_element = Ref(item)
            previous_item = item
            self.last_element = ref

    def __str__(self):
        arr = []
        self.print_element(arr, self.first_element)
        return str(arr)

    def print_element(self, arr, ref):
        if ref is not None:
            arr.append(str(ref))
            return self.print_element(arr, ref.right)
        return arr

    def add_last(self, new_item):
        previous_item = self.last_element
        ref = self.last_element = Ref(new_item, left=previous_item)
        previous_item.right = ref

    def add_first(self, new_item):
        next_item = self.first_element
        ref = self.first_element = Ref(new_item, right=next_item)
        next_item.left = ref

    def get_first(self):
        return self.first_element.item

    def get_last(self):
        return self.last_element.item

    def remove_first(self):
        self.first_element = self.first_element.right
        self.first_element.left = None

    def remove_last(self):
        self.last_element = self.last_element.left
        self.last_element.right = None

    def remove(self, item):
        pass


class Ref:

    def __init__(self, item, left=None, right=None):
        self.item = item
        self.left = left
        self.right = right

    def __str__(self):
        return f'Ref({Ref.__get_value(self.left)}-{self.item}-{Ref.__get_value(self.right)})'

    @staticmethod
    def __get_value(ref):
        if ref is not None:
            return ref.item
        else:
            return None


linkList = LinkList(1, 2, 3, 4, 5)

print(linkList)
linkList.add_last(6)
linkList.add_last(7)
linkList.add_first(0)
linkList.add_first(-1)
print(linkList)

print(f'get first: {linkList.get_first()}')
print(f'get last: {linkList.get_last()}')

linkList.remove_last()
print("removed last one")

print(linkList)

linkList.remove_first()
print("removed first one")

print(linkList)
