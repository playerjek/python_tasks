from player import Player


class PlayerBuilder:

    def __init__(self):
        self.player = Player()

    def with_accuracy(self, accuracy):
        self.player.accuracy = accuracy
        return self

    def with_nickname(self, nickname):
        self.player.nickname = nickname
        return self

    def with_team_name(self, team_name):
        self.player.team_name = team_name
        return self

    def with_win_rate(self, win_rate):
        self.player.win_rate = win_rate
        return self

    def with_amount_of_prizes(self, amount_of_prizes):
        self.player.amount_of_prizes = amount_of_prizes
        return self

    def build(self):
        return self.player


player = PlayerBuilder()\
    .with_accuracy(12)\
    .with_nickname('john')\
    .with_team_name("team #1")\
    .with_amount_of_prizes(1)\
    .with_win_rate(50)\
    .build()

print(vars(player))
print(player.nickname)