from patterns.proxy_winner import ProxyWinner
from patterns.winner import Winner

winner = ProxyWinner(Winner)

winner.set_new_place(5)
winner.set_new_place(-3)