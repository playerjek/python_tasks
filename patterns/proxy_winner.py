from patterns.winner import Winner


class ProxyWinner(Winner):

    def __init__(self, winner):
        super().__init__()
        self.winner = winner

    def set_new_place(self, place):
        print(f"Previous place: {self.place}")
        print(f"New place: {place}")

        if place < 1:
            print("It's not possible to set this place!")
        else:
            self.winner.place = place
            print("Place updated!")
